import { ObjectBase } from '@awesome-nodes/object';
import { MulticastDelegate } from '@awesome-nodes/object/model';
import { Observable } from 'rxjs';


export interface IService<T>
{
    backgroundWorkers: Map<string, Observable<T>>;
}

/**
 * Represents the base object for the service in the service provider pattern.
 */
export abstract class ServiceBase<T> extends ObjectBase implements IService<T>
{
    protected _backgroundWorkers = new Map<string, Observable<T>>();

    //region Public Properties

    /**
     * Returns currently active background workers of this instance.
     * @returns {Map<string, Observable<T>>}
     */
    public get backgroundWorkers(): Map<string, Observable<T>>
    {
        return this._backgroundWorkers;
    }

    /**
     * Returns a boolean value which indicates if this service instance contains active background workers.
     * @returns {boolean}
     */
    public get isBusy(): boolean
    {
        return !!this._backgroundWorkers.size;
    }

    //endregion

    // noinspection JSUnusedGlobalSymbols
    protected createBackgroundWorker(observable: Observable<T>, workerName: string): Observable<T>
    {
        const backgroundWorker = MulticastDelegate.multicast<T>(observable);
        backgroundWorker.subscribe({
            complete: () => this._backgroundWorkers.delete(workerName),
            error   : () => this._backgroundWorkers.delete(workerName),
        });
        this._backgroundWorkers.set(workerName, backgroundWorker);

        return backgroundWorker;
    }

}
