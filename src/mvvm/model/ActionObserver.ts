import { EventArgs, IEvent } from '@awesome-nodes/object';
import { Disposable } from 'mvvm/model/Disposable';
import { ObjectModel } from 'mvvm/model/ObjectModel';
import { BehaviorSubject, Observable, PartialObserver, Subscription, throwError } from 'rxjs';
import { Nullable } from 'simplytyped';


export interface IPartialActionObserver<T, TEventArgs extends EventArgs = EventArgs>
{
    init?: () => TEventArgs;
    next?: (value: T) => TEventArgs;
    error?: (error: Error) => void;
    complete?: () => TEventArgs;
}

/**
 * A object model utility for watching observables using command pattern style handler functions
 * which are bound to the provided {@see ActionObserver.#_thisArg} argument.
 * @template T
 */
export class ActionObserver<T> extends Disposable
{
    //region Instance Members

    readonly #_thisArg: ObjectModel;
    #_actionObservable?: Observable<T>;
    #_actionSubscription?: Subscription;
    readonly #_nextResult: BehaviorSubject<T>;
    readonly #_nextResult$: Observable<T>;
    readonly #_actionResult: BehaviorSubject<T>;
    readonly #_actionResult$: Observable<T>;
    #_onProgress?: IEvent<ObjectModel>;
    #_onError?: (e: Error) => void;
    #_onComplete?: IEvent<ObjectModel>;

    //endregion

    //region Public Properties

    //region Action Observable

    /**
     * Returns the observable returned from the last observed action.
     * @description Subscribe to observe the latest observed action.
     */
    public get action(): Nullable<Observable<T>>
    {
        return this.#_actionObservable;
    }

    /**
     * Returns the last 'next' result from the last observed action.
     * @description Subscribe to receive the final 'next' result of any called action.
     */
    public get actionResult(): Observable<T>
    {
        return this.#_actionResult$;
    }

    /**
     * Returns the last 'next' result value from the last observed action.
     * @description Bind to receive the final 'next' result value of any called action.
     */
    public get actionResultValue(): T
    {
        return this.#_actionResult.getValue();
    }

    /**
     * Returns a boolean value indicating weather this instance has ever received a result
     * other than the `ActionObserver._defaultResult` from an action.
     */
    public get actionResultAvailable(): boolean
    {
        return !!this.actionResultValue && this.actionResultValue !== this._defaultResult;
    }

    //endregion

    //region Next Result Observable

    /**
     * Returns the 'next' result from the currently observed action.
     * @description Subscribe to receive the 'next' result of any called action.
     */
    public get nextResult(): Observable<T>
    {
        return this.#_nextResult$;
    }

    /**
     * Returns the 'next' result value from the currently observed action.
     * @description Bind to receive the 'next' result value of any called action.
     */
    public get nextValue(): T
    {
        return this.#_nextResult.getValue();
    }

    /**
     * Returns a boolean value indicating weather this instance has ever received a result
     * other than the `ActionObserver._defaultResult` from an action.
     */
    public get nextResultAvailable(): boolean
    {
        return !!this.nextValue && this.nextValue !== this._defaultResult;
    }

    //endregion

    /**
     * Sets a command pattern style function used to observe {@link NextObserver.next} events.
     * @param {IEvent<ObjectModel>} value
     */
    public set onProgress(value: IEvent<ObjectModel>)
    {
        this.#_onProgress = this.#_thisArg ? value.bind(this.#_thisArg) : value;
    }

    /**
     * Sets an error handler function used to observe {@link ErrorObserver.error} events.
     * Note: The error handler function is not bound to this instance.
     * @param {IEvent<ObjectModel>} value
     */
    public set onError(value: (e: Error) => void)
    {
        this.#_onError = value;
    }

    /**
     * Sets a command pattern style function used to observe {@link CompletionObserver.complete} events.
     * @param {IEvent<ObjectModel>} value
     */
    public set onComplete(value: IEvent<ObjectModel>)
    {
        this.#_onComplete = this.#_thisArg ? value.bind(this.#_thisArg) : value;
    }

    //endregion

    /**
     *
     * @param _thisArg If provided, all observer functions are invoked on except the error observer.
     * @param _defaultResult if provided, it will be used as default value for the
     */
    public constructor(_thisArg?: ObjectModel, private readonly _defaultResult?: T)
    {
        super();
        this.#_thisArg       = _thisArg || this;
        this.#_nextResult    = new BehaviorSubject<T>(_defaultResult!);
        this.#_nextResult$   = this.#_nextResult.asObservable();
        this.#_actionResult  = new BehaviorSubject<T>(_defaultResult!);
        this.#_actionResult$ = this.#_actionResult.asObservable();
    }

    /**
     * Observes actions by additionally using a provided next observer function or `PartialActionObserver` instance.
     * @param action
     * @param observer
     */
    public observe<TSender extends ObjectModel, TEventArgs extends EventArgs>(
        action: () => Observable<T>,
        observer?: IEvent<TSender, TEventArgs> | IPartialActionObserver<T, TEventArgs>): Observable<T>
    {
        let observableResult = {} as PartialObserver<T>;
        if (observer) {
            if (typeof observer == 'function') {
                observableResult = {
                    next    : (result: T): void =>
                    {
                        this.#_nextResult.next(result);
                        observer.bind(this.#_thisArg)(this.#_thisArg as TSender, EventArgs.empty as TEventArgs);
                    },
                    complete: (): void =>
                    {
                        this.#_actionResult.next(this.#_nextResult.getValue());
                    },
                };
            } else {
                if (this.#_onProgress && observer.init)
                    this.#_onProgress(this.#_thisArg, observer.init());

                observableResult.error = (e: Error): void =>
                {
                    if (observer.error)
                        observer.error(e);
                    else if (this.#_onError)
                        this.#_onError(e);
                    else
                        console.error(e);
                };

                observableResult.next = (result: T) =>
                {
                    if (result instanceof Error) {
                        //eslint-disable-next-line @typescript-eslint/no-explicit-any
                        (observableResult as any).error(result);

                        return;
                    }
                    this.#_nextResult.next(result);
                    if (this.#_onProgress && observer.next)
                        this.#_onProgress(this.#_thisArg, observer.next(result));
                };

                observableResult.complete = () =>
                {
                    this.#_actionResult.next(this.#_nextResult.getValue());
                    if (this.#_onComplete && observer.complete)
                        this.#_onComplete(this.#_thisArg, observer.complete());
                };
            }
        }
        try {
            this.#_actionObservable = action();
        }
        catch (exception) {
            (this.#_onError || console.error)(exception as Error);
            this.#_actionObservable = throwError(exception);
        }

        if (observer)
            this.#_actionSubscription = this.#_actionObservable.subscribe(observableResult);

        return this.#_actionObservable;
    }

    public cancel(): void
    {
        this.#_actionSubscription && !this.#_actionSubscription.closed && this.#_actionSubscription.unsubscribe();
    }

    public reset(): void
    {
        this.#_nextResult.next(this._defaultResult!);
        this.#_actionResult.next(this._defaultResult!);
    }

    /** @inheritDoc */
    public equals(other: ActionObserver<T>): boolean
    {
        return super.equals(other) && this.#_actionResult === other.#_actionResult;
    }

    /** @inheritDoc */
    //tslint:disable-next-line:no-flag-args
    public dispose(disposing?: boolean): void
    {
        if (disposing && !this.isDisposed) {
            this.cancel();
            this.#_nextResult.unsubscribe();
            this.#_actionResult.unsubscribe();
        }
        super.dispose(disposing);
    }
}
