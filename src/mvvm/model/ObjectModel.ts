import { ObjectBase } from '@awesome-nodes/object';


/**
 * Represents the base object of all objects in the model-view-view-model pattern.
 */
export abstract class ObjectModel extends ObjectBase
{
}
