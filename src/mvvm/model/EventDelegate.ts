/* eslint-disable @typescript-eslint/no-use-before-define */
import { InjectionScope, InjectionToken } from '@awesome-nodes/injection-factory';
import { EventArgs, IEvent, IEventDelegate } from '@awesome-nodes/object';
import { IDelegate } from '@awesome-nodes/object/model';
import { ObjectModel } from 'mvvm/model/ObjectModel';
import { Subscription } from 'rxjs';


export interface IEventObserver<TSender extends ObjectModel, TEventArgs extends EventArgs = EventArgs>
{
    next: IEvent<TSender, TEventArgs>;
    error?: (err: unknown) => void;
    complete?: () => void;
}

export interface IEventEmitter<TSender extends ObjectModel = ObjectModel, TEventArgs extends EventArgs = EventArgs>
{
    //tslint:disable-next-line:no-flag-args
    new(async?: boolean): this;

    emit(delegate: IEventDelegate<TSender, TEventArgs>): void;

    subscribe(
        next?: (value: IEventDelegate<TSender, TEventArgs>) => void,
        error?: (error: unknown) => void,
        complete?: () => void): Subscription;
}

/**
 * Represents a simple event delegate used to encapsulate any web frontend framework event emitter object.
 * @template TSender, TEventArgs
 */
export class EventDelegate<TSender extends ObjectModel, TEventArgs extends EventArgs = EventArgs>
    implements IDelegate<TSender, TEventArgs>
{
    private readonly _subscriptions  = new Map<IEvent<TSender, TEventArgs>, Subscription>();
    protected readonly _eventEmitter = new EVENT_EMITTER_TOKEN.value() as unknown as IEventEmitter<TSender, TEventArgs>;

    /**
     * Returns the wen framework EventEmitter used only within angular components.
     * @access Any access from a view-model, model or service is not
     * permitted because it couples angular code to the object model.
     */
    public get eventEmitter(): IEventEmitter<TSender, TEventArgs>
    {
        return this._eventEmitter;
    }

    /**
     * Initializes this instance
     * @param {TSender} _producer The event producer to be used as `thisArg` for the command handler invocation.
     */
    public constructor(protected _producer: TSender)
    {}

    /**
     * Subscribes to the underlying EventEmitter observable and delegates event
     * subscription to commend pattern style handler functions.
     * @param observer The next observer function or an event observer instance.
     * @param thisArg If provided, all observer functions are invoked on except the error observer.
     */
    public subscribe(
        observer: IEvent<TSender, TEventArgs> | IEventObserver<TSender, TEventArgs>,
        thisArg?: object): Subscription
    {
        let next: IEvent<TSender, TEventArgs>;
        let error: ((err: unknown) => void) | undefined;
        let complete: (() => void) | undefined;

        if (typeof observer == 'function')
            next = observer;
        else
            ({ next, error, complete } = observer);

        if (this._subscriptions.get(next))
            this.unsubscribe(next);

        const subscription = this._eventEmitter.subscribe(
            (delegate: IEventDelegate<TSender, TEventArgs>) =>
            {
                delegate(thisArg ? next.bind(thisArg) : next);
            },
            error && thisArg ? error.bind(thisArg) : error,
            complete && thisArg ? complete.bind(thisArg) : complete,
        );
        subscription.add(() =>
        {
            this._subscriptions.delete(next);
        });
        this._subscriptions.set(next, subscription);

        return subscription;
    }

    public unsubscribe(observer: IEvent<TSender, TEventArgs> | IEventObserver<TSender, TEventArgs>): void
    {
        const subscription = this._subscriptions.get(typeof observer == 'function' ? observer : observer.next);
        //noinspection PointlessBooleanExpressionJS subscription.closed is undefined if not raised
        if (subscription && subscription.closed === false)
            subscription.unsubscribe();
    }

    public invoke(ea: TEventArgs = EventArgs.empty as TEventArgs): void
    {
        this._eventEmitter.emit((delagate: IEvent<TSender, TEventArgs>, thisArg?: object) =>
        {
            (thisArg ? delagate.bind(thisArg) : delagate)(this._producer, ea);
        });
    }
}

export const EVENT_EMITTER_TOKEN: InjectionToken<IEventEmitter> = InjectionScope
    .get(EventDelegate, null).addToken('event-emitter');
