import { EventArgs } from '@awesome-nodes/object/model';
import { EventDelegate } from 'mvvm/model/EventDelegate';
import { IDisposable } from 'mvvm/model/IDisposable';
import { ObjectModel } from 'mvvm/model/ObjectModel';


/**
 * Represents a disposable object for single threaded environments.
 */
export class Disposable extends ObjectModel implements IDisposable
{
    #_isDisposed         = false;
    readonly #_disposing = new EventDelegate<IDisposable>(this);
    readonly #_disposed  = new EventDelegate<IDisposable>(this);

    //tslint:disable-next-line:newspaper-order
    protected onDisposing(args: EventArgs): void
    {
        this.#_disposing.invoke(args);
    }

    protected onDisposed(args: EventArgs): void
    {
        this.#_disposed.invoke(args);
    }

    //<editor-fold desc="IDisposable Members">

    /** @inheritDoc */
    //tslint:disable-next-line:no-flag-args
    public dispose(disposing?: boolean): void
    {
        if (disposing === undefined && !this.#_isDisposed) {
            this.onDisposing(EventArgs.empty);
            this.dispose(true);
            this.onDisposed(EventArgs.empty);

            return;
        }
        this.#_isDisposed = true;
    }

    /** @inheritDoc */
    public get disposing(): EventDelegate<IDisposable>
    {
        return this.#_disposing;
    }

    /** @inheritDoc */
    public get disposed(): EventDelegate<IDisposable>
    {
        return this.#_disposed;
    }

    /** @inheritDoc */
    public get isDisposed(): boolean
    {
        return this.#_isDisposed;
    }

    //</editor-fold>
}
