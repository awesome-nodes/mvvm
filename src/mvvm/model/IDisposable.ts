import { EventDelegate } from 'mvvm/model/EventDelegate';
import { ObjectModel } from 'mvvm/model/ObjectModel';


/**
 * Defines a disposable object which provides disposal lifecycle events.
 */
export interface IDisposable extends ObjectModel
{
    /**
     * Returns a value indicating if the current object instance has been disposed.
     */
    isDisposed: boolean;

    /**
     * Raised on before this instance gets disposed.
     */
    disposing: EventDelegate<IDisposable>;

    /**
     * Raised after this instance has been disposed.
     */
    disposed: EventDelegate<IDisposable>;

    /**
     * Releases all unmanaged resources of this instance. Please call this method
     * only without providing the 'disposing' parameter (explained within parameter doc).
     * Note: When overridden within an implementing class then call the dispose method of
     * the superclass always at least (after all own disposal logic has been processed).
     * @param {boolean} disposing Note: This parameter is for internal usage only and used within derived
     * implementations to indicate if a regular disposal is in progress prior object finalization.
     */
    //tslint:disable-next-line:no-flag-args
    dispose(disposing?: boolean): void;
}

/**
 * Disposable context definition statement markup for IDisposable implementations.
 * @param {T} resource
 * @param {(resource: T) => void} func
 * @template T
 */
export function using<T extends IDisposable>(resource: T, func: (resource: T) => void): void
{
    try {
        func(resource);
    }
    finally {
        resource.dispose();
    }
}
