import { EventArgs } from '@awesome-nodes/object';
import { EVENT_EMITTER_TOKEN, EventDelegate, IEventEmitter } from 'mvvm/model/EventDelegate';
import { ObjectModel } from 'mvvm/model/ObjectModel';


/** @inheritDoc */
export class AsyncEventDelegate<TSender extends ObjectModel, TEventArgs extends EventArgs = EventArgs>
    extends EventDelegate<TSender, TEventArgs>
{
    protected readonly _eventEmitter = new
    EVENT_EMITTER_TOKEN.value(true) as unknown as IEventEmitter<TSender, TEventArgs>;
}
