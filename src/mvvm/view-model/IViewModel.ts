import { ObjectModel } from 'mvvm/model';


/**
 * Defines the event lifecycle hook signature for every view-model object.
 */
export interface IViewModel<T extends ObjectModel = ObjectModel> extends ObjectModel
{
    name: string;
    model: T;

    /**
     * A lifecycle hook that is called after web frontend framework has initialized
     * all data-bound properties of a directive.
     */
    init?(): void;

    /**
     * A lifecycle hook that is called after web frontend framework has fully initialized a component's view.
     */
    viewInit?(): void;

    /**
     * A lifecycle hook that is called after the default change
     * detector has completed checking a component's view for changes.
     */
    viewChecked?(): void;

    /**
     * A lifecycle hook that is called after web frontend framework has fully initialized all content of a directive.
     * When implemented in a derived class, it can be used to handle any additional initialization tasks.
     */
    contentInit?(): void;

    /**
     * A lifecycle hook that is called after the default change
     * detector has completed checking all content of a directive.
     */
    contentChecked?(): void;

    /**
     * A lifecycle hook that is called when any data-bound property of a directive changes.
     * @param changes The changed properties.
     */
    changes?(changes: unknown): void;

    /**
     * A lifecycle hook that is called when a directive, pipe, or service is destroyed.
     * Use for any custom cleanup that needs to occur when the instance contains unmanaged resources.
     */
    destroy?(): void;
}
