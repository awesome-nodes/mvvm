import { ObjectBase } from '@awesome-nodes/object';
import { Disposable, IDisposable, ObjectModel } from 'mvvm/model';
import { IViewModel } from 'mvvm/view-model';


/**
 * Represents the base for all view models in the model-view-view-model pattern.
 * @template TModel
 */
export abstract class ViewModelBase<TModel extends ObjectModel> extends ObjectBase implements IViewModel<TModel>
{
    private _model: TModel;

    //region Public Perperties

    public get name(): string
    {
        return this.toString();
    }

    /**
     * Returns the model instance which is associated with this view-model instance.
     */
    public get model(): TModel
    {
        return this._model;
    }

    /**
     * Returns the model instance which is associated with this view-model instance.
     */
    public set model(value: TModel)
    {
        this._model = value;
    }

    //endregion

    /**
     * Initializes this instance.
     * @param {TModel} model
     * @param {string} name
     */
    protected constructor(model: TModel, name?: string)
    {
        super(name);
        this._model = model;
    }

    /** @inheritDoc */
    public init?(): void;

    /** @inheritDoc */
    public viewInit?(): void;

    /** @inheritDoc */
    public viewChecked?(): void;

    /** @inheritDoc */
    public contentInit?(): void;

    /** @inheritDoc */
    public contentChecked?(): void;

    /** @inheritDoc */
    public changes?(changes: unknown): void;

    /** @inheritDoc */
    public destroy(): void
    {
        this.model && (this.model instanceof Disposable
            || (this.model as unknown as IDisposable).dispose)
        && (this.model as unknown as IDisposable).dispose();
    }

    /**
     * Checks equality by comparing the name property of this instance.
     * @inheritDoc
     * @param {ViewModelBase<TModel>} other
     */
    public equals(other: ViewModelBase<TModel>): boolean
    {
        return super.equals(other) &&
            this.name == other.name;
    }
}
