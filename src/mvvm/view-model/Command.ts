import { EventArgs } from '@awesome-nodes/object';
import { IViewModel } from 'mvvm/view-model';
import { Observable } from 'rxjs';


/**
 * Command binding method signature for View-Model Command invocations using the command pattern. Use this signature to
 * declare commands within view-models providing a configurable parameter layout for the event handler delegate.
 * Important Notice: Implement command invocations only from within view-models or web framework components only but
 * never directly from within the view! Otherwise you will break the mvvm design pattern.
 * @example
 * TODO(docs) Write documentation examples for Command usage
 */
export type Command<TParams extends ICommandParameters = ICommandParameters, TResult = void> =
/**
 *
 * @param params Acts as the parameter store for the event handler invocation arguments.
 * @returns Can return a `Promise` which itself can return a `Observable` used to append presentation and workflow logic
 * to the command action.
 */
    (params: TParams) => void | Promise<void | Observable<void | TResult>>;

/**
 * Command binding parameters base signature with event sender definition.
 */
export interface ICommandParameters<T extends IViewModel = IViewModel>
{
    sender: T;
}

/**
 * Command binding parameter signature with event arguments definition.
 */
export interface ICommandHandler<TArgs extends EventArgs = EventArgs, TViewModel extends IViewModel = IViewModel>
    extends ICommandParameters<TViewModel>
{
    eventArgs: TArgs;
}
