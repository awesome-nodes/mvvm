import { ClassProviderConfig, FactoryProviderConfig, Token } from '@awesome-nodes/injection-factory';
import { ObjectBase } from '@awesome-nodes/object';
import { ENVIRONMENT_TOKEN } from '@awesome-nodes/object/data';
import { ServiceBase } from 'mvvm/services';
import { ConstructorFunction } from 'simplytyped';


/**
 * Configures the factory provider to return the service factory configuration.
 */
export interface IServiceProvider
{
    /**
     * An injection token. (Typically an instance of `Type` or `InjectionToken`, but can be `any`).
     */
    provide?: ConstructorFunction<object>;

    /**
     * A list of `token`s which need to be resolved by the injector. The list of values is then
     * used as arguments to the `useFactory` function.
     */
    deps?: Array<Token>;
}

/**
 * Represents the base object for the provider in the service provider pattern.
 */
export abstract class ServiceProviderBase<T extends ServiceBase<unknown>> extends ObjectBase
{
    public abstract get designFactory(): IServiceProvider;

    public abstract get developmentFactory(): IServiceProvider;

    public abstract get productionFactory(): IServiceProvider;

    public provide(provide: ConstructorFunction<T>): T
    {
        const factoryProvider = (this.constructor as typeof ServiceProviderBase)
            .createFactoryProvider<T>(provide, this);

        if (!factoryProvider.provide)
            factoryProvider.provide = provide;

        return this.inject(
            {
                provide   : factoryProvider.provide,
                useFactory: factoryProvider.useFactory,
                deps      : factoryProvider.deps,
            },
        );
    }

    public abstract inject(provider: FactoryProviderConfig<T>): T;

    /**
     * Returns the factory configuration provided by a derived provider type on which this function was invoked on.
     * @param provide
     * @param provider
     */
    public static createFactoryProvider<U extends ServiceBase<unknown>>(
        provide: ConstructorFunction<U>,
        provider: ServiceProviderBase<U> = new (this as unknown as ConstructorFunction<ServiceProviderBase<U>>)()):
        FactoryProviderConfig<U>
    {
        let factoryProvider: IServiceProvider;
        switch (ENVIRONMENT_TOKEN.value) {
            case null:
            case undefined:
                factoryProvider = provider.designFactory;
                break;
            case false:
                factoryProvider = provider.developmentFactory;
                break;
            default:
                factoryProvider = provider.productionFactory;
        }

        if (!factoryProvider.provide)
            factoryProvider.provide = provide;

        return {
            provide   : factoryProvider.provide,
            useFactory: ((...deps: Array<unknown>): object =>
            {
                return new ((factoryProvider.provide as ConstructorFunction<object>).bind(null, ...deps))();
            }),
            deps      : factoryProvider.deps || [],
        };
    }

    /**
     * Returns the staging configuration provided by the type on which this function was invoked on.
     */
    public static createClassProvider<U extends ServiceBase<unknown>>(): ClassProviderConfig<U>
    {
        const providerBase      = new (this as unknown as ConstructorFunction<ServiceProviderBase<U>>)();
        const productionFactory = providerBase.productionFactory;
        let factoryProvider: IServiceProvider;

        switch (ENVIRONMENT_TOKEN.value) {
            case null:
            case undefined:
                factoryProvider = providerBase.designFactory;
                break;
            case false:
                factoryProvider = providerBase.developmentFactory;
                break;
            default:
                factoryProvider = productionFactory;
        }

        return {
            provide : productionFactory.provide as Token<U>,
            useClass: factoryProvider.provide as ConstructorFunction<U>,
        };
    }
}
