import { ObjectModel } from 'mvvm/model';
import { IViewModel } from 'mvvm/view-model';


/**
 * Represents the object base for all ngx components within the model-view-view-model pattern.
 * @template TViewModel, TModel
 */
//tslint:disable-next-line:min-class-cohesion
export abstract class ComponentBase<TViewModel extends IViewModel<TModel>, TModel extends ObjectModel = ObjectModel>
{
    //region Public Properties

    /**
     * Returns the view-model associated with this component instance.
     */
    //tslint:disable-next-line:newspaper-order
    public get vm(): TViewModel
    {
        return this._vm;
    }

    /**
     * Returns the model instance from the view-model associated with this component instance.
     * @returns {TModel}
     */
    public get model(): TModel
    {
        return this.vm.model;
    }

    /**
     * Sets the model instance for the view-model associated with this component instance.
     * @param {TModel} value
     */
    public set model(value: TModel)
    {
        this.vm.model = value;
    }

    //endregion

    /**
     * Initialized this component base instance.
     * @param {TViewModel} _vm
     */
    protected constructor(private readonly _vm: TViewModel)
    {}
}
